# Author: Gor G.
# MIT License

import numpy as np
import cv2; cv = cv2
from matplotlib import pyplot as plt
from matplotlib import collections  as mc

def drawVerticalMatches(img1, keypoints1, img2, keypoints2, matches1to2):
    assert(img1.shape == img2.shape)
    
    newImg = np.vstack((img1, img2))
    lines = ([(keypoints1[i.queryIdx].pt), 
              (keypoints2[i.trainIdx].pt[0],keypoints2[i.trainIdx].pt[1] +img2.shape[0])
              ]
             for i in matches1to2)
    
    c = np.hstack((np.random.uniform(size = (len(matches1to2), 3 )), 0.5*np.ones((len(matches1to2), 1))))
    lc = mc.LineCollection(lines, colors=c, linewidths=1)
    
    
    fig, ax = plt.subplots()
    ax.imshow(newImg, cmap='gray')
    
    ax.add_collection(lc)
    plt.tight_layout()
    plt.show()
